import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ItemsService {
  items: any[] = [];

  constructor() {}

  getItems(): any[] {
    return this.items;
  }

  createItem(newItem: any) {
    this.items.push(newItem);
  }
}
