import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lab-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() id: number;
  @Input() content: any;

  constructor() {

  }

  ngOnInit() {
    console.log(this.id);
    console.log(this.content);
  }

}
