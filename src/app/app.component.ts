import { ItemsService } from "./items/items.service";
import { Component } from "@angular/core";

@Component({
  selector: "lab-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "labo2";

  items: any[];
  newItem = {
    title: "",
    description: "",
    image: "",
    date: ""
  };

  constructor(private itemsService: ItemsService) {}

  ngOnInit() {
    this.items = this.itemsService.getItems();
  }

  addNewItem() {
    this.itemsService.createItem(this.newItem);
    this.newItem = {
      title: "",
      description: "",
      image: "",
      date: ""
    };
  }
}
